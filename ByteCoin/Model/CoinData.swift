//
//  CoinData.swift
//  ByteCoin
//
//  Created by Paweł on 16/06/2020.
//
//

import Foundation

struct CoinData: Decodable {
    let rate: Double
}
